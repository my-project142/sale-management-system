CREATE PROCEDURE usp_Products_InsertIntoproductSizes
(
    @ProductID INT
	,@SizeID INT
)
AS
BEGIN

INSERT INTO [dbo].[ProductSizes]
           ([ProductID]
		   ,[SizeID]
		   )
VALUES
(
     @ProductID
	 ,@SizeID
)
END