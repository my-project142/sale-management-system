CREATE PROCEDURE usp_ListTypeData_LoadDataintoComboboxes
(
  @listTypeID INT
)
AS
BEGIN
  SELECT [ListTypeDataID] AS 'ID'
         ,[Description] AS 'Description'
		 FROM [dbo].[ListTypeData]
		 WHERE [ListTypeID] = @listTypeID
  END