USE [Salesdata]
GO
/****** Object:  StoredProcedure [dbo].[ups_Products_LoadAllProductsbyProductName]    Script Date: 12/14/2021 1:22:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[ups_Products_LoadAllProductsbyProductName]
(
    @ProductName NVARCHAR(100)
)
AS 
BEGIN
SELECT
   p.[ProductID]
   ,p.[ProductName] AS 'ProductName'
   ,l1.[Description] AS 'CategoryID' 
   ,l2.[Description] AS 'SupplierID'
   ,p.[PurchasePrice] AS 'PurchasePrice'
   ,p.[SalesPrice] AS 'SalesPrice'
   ,l3.[Description] AS 'Size'
   
   FROM [dbo].[Products] p
   LEFT JOIN [dbo].[ListTypeData] l1 ON p.CategoryID = l1.ListTypeDataID
   LEFT JOIN [dbo].[ListTypeData] l2 ON p.SupplierID = l2.ListTypeDataID
   LEFT JOIN  [dbo].[ProductSizes] ps ON p.ProductID = ps.[ProductID]
   LEFT JOIN [dbo].[ListTypeData] l3 ON ps.[SizeID] = l3.ListTypeDataID
      
	  WHERE p.[ProductName] LIKE +'%' + @ProductName +'%'
   END
