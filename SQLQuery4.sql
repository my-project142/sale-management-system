USE [Salesdata]
GO
/****** Object:  StoredProcedure [dbo].[usp_Add_Stocks]    Script Date: 12/15/2021 10:20:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_Add_Stocks]
(
            @Date DATETIME
           ,@ProductID INT
           ,@ProductName NVARCHAR(50)
           ,@Quantity DECIMAL(18,2)
           ,@SalesPrice DECIMAL(18,2)
)
AS
BEGIN

INSERT INTO [dbo].[Stock]
           ([Date]
           ,[ProductID]
           ,[ProductName]
           ,[Quantity]
           ,[SalesPrice])
     VALUES (
	        @Date 
           ,@ProductID
           ,@ProductName
           ,@Quantity
           ,@SalesPrice)
		   SELECT SCOPE_IDENTITY()
		   END
